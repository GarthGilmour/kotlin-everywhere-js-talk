import org.w3c.dom.HTMLButtonElement
import org.w3c.dom.HTMLElement
import kotlin.browser.document

@JsModule("axios")
@JsNonModule
external fun get(url: String): dynamic

interface Course {
    val id: String
    val title: String
    val difficulty: String
    val duration: Int
}

fun loadCourses(parent: HTMLElement) {
    println("Fetching courses from service")
    get("http://localhost:9090/courses")
        .then { response ->
            val courses = response.data as Array<Course>
            courses.forEach { addCourseToHtmlList(it, parent) }
        }
}

fun addCourseToHtmlList(it: Course, parent: HTMLElement) {
    val text = "\tCourse ${it.id} with title ${it.title}\n"
    val item = document.createElement("li")
    item.appendChild(document.createTextNode(text))
    parent.appendChild(item)
}

fun main() {
    val display = document.getElementById("coursesDisplay") as HTMLElement
    val button = document.getElementById("coursesButton") as HTMLButtonElement
    button.onclick = { loadCourses(display) }
}
