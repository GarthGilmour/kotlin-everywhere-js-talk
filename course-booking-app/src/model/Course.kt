package model

interface Course {
    val id: String
    val title: String
    val difficulty: String
    val duration: String
}
