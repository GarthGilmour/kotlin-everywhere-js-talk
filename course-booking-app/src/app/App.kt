package app

import courses_table.coursesTable
import kotlinx.html.js.onClickFunction
import react.*
import react.dom.*
import services.CoursesServiceAxios

class App : RComponent<RProps, RState>() {
    private val coursesService = CoursesServiceAxios()

    override fun RBuilder.render() {
        div {
            h1 {
                +"React with Kotlin"
            }
            coursesTable {
                attrs {
                    service = coursesService
                }
            }
        }
    }
}

fun RBuilder.app() = child(App::class) {}
