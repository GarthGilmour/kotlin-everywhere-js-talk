package services

import axios.AxiosConfigSettings
import axios.AxiosResponse
import kotlinext.js.jsObject
import model.Course
import kotlin.js.Promise
import kotlin.js.json

@JsModule("axios")
external fun <T> axios(config: AxiosConfigSettings): Promise<AxiosResponse<T>>

class CoursesServiceAxios : CoursesService {
    private fun settings(urlStr: String): AxiosConfigSettings {
        fun buildHeaders() = json(
                "Accept" to "application/json",
                "Content-Type" to "application/json")

        return jsObject {
            url = urlStr
            method = "GET"
            headers = buildHeaders()
        }
    }
    override fun allCourses(handler: (Array<Course>) -> Unit) {
        axios<Array<Course>>(settings("http://localhost:9090/courses")).then { response ->
            println("Fetched courses from server")
            handler(response.data)
        }
    }
}
