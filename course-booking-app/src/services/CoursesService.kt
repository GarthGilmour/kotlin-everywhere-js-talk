package services

import model.Course

interface CoursesService {
    fun allCourses(handler: (Array<Course>) -> Unit)
}
