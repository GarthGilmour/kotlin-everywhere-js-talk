package courses_table

import kotlinx.html.js.onClickFunction
import model.Course
import org.w3c.dom.events.Event
import react.RBuilder
import react.RComponent
import react.RHandler
import react.dom.*
import react.setState

fun RBuilder.coursesTable(handler: RHandler<CoursesTableProps>) = child(CoursesTable::class, handler)

class CoursesTable(props: CoursesTableProps) : RComponent<CoursesTableProps, CoursesTableState>(props) {
    init {
        state.courses = emptyArray()
        state.service = props.service
    }

    private fun fetchCourses(e : Event) {
        state.service.allCourses { data ->
            console.log(data)
            setState { courses = data }
        }
    }

    override fun RBuilder.render() {
        div {
            button {
                +"Fetch Courses from Server"
                attrs { onClickFunction = ::fetchCourses }
            }
            table(classes = "table table-striped table-bordered") {
                thead {
                    tr {
                        th {
                            +"ID"
                        }
                        th {
                            +"Title"
                        }
                        th {
                            +"Duration"
                        }
                        th {
                            +"Difficulty"
                        }

                    }
                }
                tbody {
                    state.courses.map { course ->
                        tr {
                            td { +course.id }
                            td { +course.title }
                            td { +course.duration }
                            td { +course.difficulty }
                        }
                    }
                }
            }
        }
    }
}
