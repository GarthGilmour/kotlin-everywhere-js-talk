package courses_table

import model.Course
import react.RState
import services.CoursesService

interface CoursesTableState : RState {
    var courses: Array<Course>
    var service: CoursesService
}
