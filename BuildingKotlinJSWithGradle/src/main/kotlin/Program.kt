import dsl.course
import kotlin.browser.document

fun showDSL() {
    val dsl = course("Coding in Kotlin") {
        module("Introduction") {
            +"Declarations and type inference"
            +"The Kotlin type system and conversions"
            +"Packages, access levels and default imports"
        }
        module("Object Orientation") {
            +"Decompiling Kotlin classes using ‘javap’"
            +"Understanding properties and backing fields"
            +"A detailed explanation of primary constructors"
        }
    }
    println("Writing Kotlin DSL to browser...")
    val element = document.getElementById("dslOutputGoesHere")
    if(element != null) {
        dsl.render(element)
    } else {
        println("Cannot find node to insert content!")
    }
}
