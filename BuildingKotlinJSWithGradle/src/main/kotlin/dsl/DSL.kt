package dsl

import org.w3c.dom.Element
import kotlin.browser.document

interface Node {
    fun render(parent: Element)
}

abstract class ContainerNode<T : Node> : Node {
    protected val children = mutableListOf<T>()

    protected fun renderChildren(parent: Element, wrapperTag: String = "div") {
        val wrapperElement = document.createElement(wrapperTag)
        children.forEach { child -> child.render(wrapperElement) }
        parent.appendChild(wrapperElement)
    }
}

class Topic(private val text: String) : Node {
    override fun render(parent: Element) {
        val item = document.createElement("li")
        item.appendChild(document.createTextNode(text))
        parent.appendChild(item)
    }
}

class Module(private val title: String) : ContainerNode<Topic>() {
    override fun render(parent: Element) {
        val header = document.createElement("h3")
        header.appendChild(document.createTextNode(title))
        parent.appendChild(header)
        return renderChildren(parent, "ol")
    }

    operator fun String.unaryPlus() = children.add(Topic(this))
}

class Course(private val title: String) : ContainerNode<Module>() {
    fun module(title: String = "Unknown Course Module",
               action: Module.() -> Unit)
            = Module(title).apply(action).also { children.add(it) }

    override fun render(parent: Element) {
        val header = document.createElement("h2")
        header.appendChild(document.createTextNode(title))
        parent.appendChild(header)
        renderChildren(parent)
    }

    override fun toString() = "Course '$title'"
}

fun course(title: String = "Unknown Course Title",
           action: Course.() -> Unit) = Course(title).apply(action)
