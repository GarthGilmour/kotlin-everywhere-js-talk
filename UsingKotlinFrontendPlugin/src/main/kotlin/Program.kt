import kotlin.browser.document

@JsModule("axios")
external fun get(url: String) : dynamic

interface Course {
    val id: String
    val title: String
    val difficulty: String
    val duration: Int
}

fun main() {
    println("Fetching courses from service")
    get("http://localhost:9090/courses")
        .then { response ->
            println("Found the following courses:")
            val courses: Array<Course> = response.data as Array<Course>
            courses.forEach {
                println("\tCourse ${it.id} with title ${it.title}")
            }
        }
}
